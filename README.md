# The packaging information for python flask menu in XiVO

This repository contains the packaging information for
[python-flask-menu](https://github.com/inveniosoftware/flask-menu).

To get a new version of python-flask-menu in the XiVO repository, set the
desired version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.
